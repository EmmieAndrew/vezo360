"use strict";
const mongoose = require("mongoose");
const image = mongoose.Schema({
    image: { type: String },
    gallery: [{ image: { type: String } },],
    id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        refPath: 'user'
    },
    imageFor: {
        type: String,
        required: false,
        enum: ['user']
    },
    baseUrl:{
        type: String,
        default:"http://18.144.63.251/vezo360/assets/images/"
    },
    createdOn: { type: Date, default: Date.now },
    updatedOn: { type: Date, default: Date.now },
});
mongoose.model("image", image);
module.exports = image;