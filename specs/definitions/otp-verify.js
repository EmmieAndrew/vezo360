module.exports = [
    {
        name: "verifyOtp",
        properties: {
            userId: {
                type: "string"
            },
            otp: {
                type: "string"
            },
            newPassword: {
                type: "string"
            },
        }
    }
];
